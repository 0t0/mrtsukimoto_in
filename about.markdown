---
layout: page
title: About
permalink: /about/
---

I'm Akshay, a system administrator at [Deeproot Linux](https://deeproot.in/). I am an active advocate and user of Free(dom) Software. Most of the computing services that i use are self-hosted. I urge you to use free, privacy respecting services as it empowers and provides an opportunity for learning.

You can contact me over <a href="mailto:akshay@mrtsukimoto.in">mail</a>. Here's my [gpg key](/gpg). 

---
0t0

