---
layout: page
title: Links
--- 
Software i use for hosting services:
- [Miniflux for RSS feed](https://miniflux.app/)
- [Nextcloud for file hosting](https://nextcloud.com)
- [Postfix SMTP](https://postfix.org)
- [Dovecot IMAP](https://miniflux.app/)
- [Gitea](https://gitea.io)
- [Jekyll static site generator](https://jekyllrb.com)

Essays:
- [Free software vs Open Source](https://www.gnu.org/philosophy/open-source-misses-the-point.en.html)
- [Importance of Free Software](https://www.gnu.org/philosophy/free-software-even-more-important.html)
- [Hackers and Painters](http://www.paulgraham.com/hp.html)

Books: 
- [1984](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four)
- [Manto](https://www.goodreads.com/book/show/7518598-manto)
- [Mayyazhipuzhayude Theerangalil](https://en.wikipedia.org/wiki/Mayyazhippuzhayude_Theerangalil)
- [Permanent Record](https://en.wikipedia.org/wiki/Permanent_Record_(autobiography))
- [Randamoozham](https://en.wikipedia.org/wiki/Randamoozham)
- [The Hitchhiker's Guide to the Galaxy](https://en.wikipedia.org/wiki/The_Hitchhiker's_Guide_to_the_Galaxy)
- [The Left Hand of Darkness](https://en.wikipedia.org/wiki/The_Left_Hand_of_Darkness)
