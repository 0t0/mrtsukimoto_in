---
layout: page
title: Pictures
---
<h2>Pictures</h2>
{%- if site.pictures.size > 0 -%}
  <ul>
    {%- for post in site.pictures -%}
    <li>
      {%- assign date_format = "%d %B,%Y" -%}
      [ {{ post.date | date: date_format }} ] <a href="{{ post.url | relative_url }}">{{ post.title | escape }}</a>
    </li>
    {%- endfor -%}
  </ul>
{%- endif -%}
