---
layout: post
title: Emma
date: 2022-01-23 01:10 +0530
---

You are the poem, moist in the wind,
Conduct this song when I sing our melody,
I remember the time when you bound our palms,
Glue left in my fingers,
The wrinkles on your cheek.

I could see from dusk till dawn,
Tiny streams from the mountain up north,
I reached its source where the currents are strong,
Rocks graphite just a show.

I climbed with your glue, day and night,
Saw the neon lights at the summit,
An astuary kept it all in a jar,
The Amazon was in my hands.

Ardor your purse,
Didn't ask for change,
winter's about to pass in this fervor of seasons.

So melt this snow and build your roads,
Emma,
I have a few letters for you.
