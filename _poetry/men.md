---
layout: post
title: Men
date: 2022-05-23 13:17 +0530
---

walking through the forest,
getting lost among trees,
sun hid behind branches,
stepped into a pool of maggots.

ground shook with my heart,
as the boots sank,
i caught a glance,
a temple with fire,
lifeline in my grasp.

i sat on its steps,
why is it blue?
crawled up the stairs,
to have a peek.

this goddess had no wings,
she had no halo.
she walked on this land,
caressing trees with her fingers,
above those withered leaves.
the bass was perfect,
and i said,
"i want to ask you out for a cup of coffee."
