---
layout: post
title: Lego
date: 2024-01-07 04:47 +0530
---

Roots on my palm,
Sand inside skull,
Drown under the heavens roar.
See no prophet,
Hear no whisper,
Resting on lucid dream,
Battered down by vacuum.

Roots on my feet,
Sand dripping, an endless stream?
This would stitch me up.
Scrape them off and flush this head,
Lets restart at checkpoint number one.

