---
layout: post
title: Love
date: 2024-01-07 04:47 +0530
---

i lived in motion,
i moved through words,
didnt find a feature,
hope the tickets didnt match.
round after round.

i walk in parks,
as i move through words,
had nothing to frame,
just giants that comfort.
round after round.

now i walk this path,
when you have nothing to hang,
why have walls,
i hide in malls,
round after round.
