---
layout: post
title: Treefingers
date: 2022-01-23 01:01 +0530
---

i closed my eyes. Few of them standing in a marsh. but they had withered and its glorious branches like roots holding  the sky together. they were black with a splash of prussian blue. i had a ticket, watched them night and day as the smoke from their roots moved on like clouds flowing around mountains. they never moved in sun and rain, storm and hail, unable to comprehend their persistance. I walk into their frame, stood infront of one and touched it. i was comfortably numb.
