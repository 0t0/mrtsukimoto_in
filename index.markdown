---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
title: 0t0
layout: home
---
<h1> <center> <a href="https://en.wikipedia.org/wiki/Nineteen_Eighty-Four">"War is peace, Freedom is Slavery, Ignorance is Strength."</a> </center></h1>
<h1> <center> <a href="https://www.gnu.org/philosophy/free-software-even-more-important.html">"Freedom means having control over your own life. If you use a program to carry out activities in your life, your freedom depends on your having control over the program. You deserve to have control over the programs you use, and all the more so when you use them for something important in your life." </a> </center> </h1>
