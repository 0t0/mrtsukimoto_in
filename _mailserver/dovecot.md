---
layout: post
title: Dovecot IMAP server
date: 2023-09-21 00:05 +0530
---


## What is Dovecot? 
Dovecot is an open source IMAP and POP3 email server.

## What is an IMAP/POP3 server? 
Both Internet Message Access Protocol (IMAP) and Post Office Protocol Version 3 (POP3) are Internet standard protocols used by email clients to retrieve email. An IMAP server typically listens on port number 143 and  IMAP over SSL/TLS (IMAPS) is assigned the port number 993. POP3 listens on port 110 and POP3 over TLS listens on port 995. 

## Difference between IMAP & POP3
When an email client connects to a server using POP, it grabs all the mail on the server. It then stores every mail locally on your device, so you can access it in your email client. Finally, it deletes the mails from the email server before disconnecting. This means that the messages only exist on the device you downloaded them to.

Email clients using IMAP leave messages on the server until the user explicitly deletes them. This and other characteristics of IMAP operation allow multiple clients to manage the same mailbox. Mails are fetched and cached locally.

## Install Dovecot and other necessery packages
  ```
  apt install dovecot-core dovecot-ldap dovecot-lmtpd dovecot-sieve \
  dovecot-managesieved dovecot-imapd dovecot-pop3d
  ```
  * Lmtpd is resposible for mail delivery
  * Sieve and Managesieve packages installs packages required by dovecot to use sieve filter language. More info on sieve can be found in [sieve.info](http://sieve.info/).
  * Dovecot also supports [Full text search](https://doc.dovecot.org/configuration_manual/fts/) with which we can search through the entrie mail including attachments. Required packages or configuration is not mentioned in this guide. 

## Configure Dovecot
Dovecot configuration is fairly straightforward as it requires minimal changes to default config, unlike postfix.  Dovecot have fragmented its config files. Hence each service, be it LMTP or LDAP, is configured in its own file. This improves readability and ease of configuration. Configuration files are present in **/etc/dovecot**. Check tree listed below for location of files mentioned in confguration. 

  ```
  /etc/dovecot
  |-- conf.d
  |   |-- 10-auth.conf
  |   |-- 10-director.conf
  |   |-- 10-logging.conf
  |   |-- 10-mail.conf
  |   |-- 10-master.conf
  |   |-- 10-ssl.conf
  |   |-- 15-lda.conf
  |   |-- 15-mailboxes.conf
  |   |-- 20-imap.conf
  |   |-- 20-lmtp.conf
  |   |-- 20-managesieve.conf
  |   |-- 20-pop3.conf
  |   |-- 90-acl.conf
  |   |-- 90-plugin.conf
  |   |-- 90-quota.conf
  |   |-- 90-sieve.conf
  |   |-- 90-sieve-extprograms.conf
  |   |-- auth-checkpassword.conf.ext
  |   |-- auth-deny.conf.ext
  |   |-- auth-dict.conf.ext
  |   |-- auth-ldap.conf.ext
  |   |-- auth-master.conf.ext
  |   |-- auth-passwdfile.conf.ext
  |   |-- auth-sql.conf.ext
  |   |-- auth-static.conf.ext
  |   |-- auth-system.conf.ext
  |   |-- auth-vpopmail.conf.ext
  |-- dovecot.conf
  |-- dovecot-dict-auth.conf.ext
  |-- dovecot-dict-sql.conf.ext
  |-- dovecot-ldap.conf.ext
  |-- dovecot-sql.conf.ext
  ```

You can view all the services running in dovecot using **netstat** command

  ```
  $ netstat -tnpl | grep dovecot
  tcp        0      0 0.0.0.0:4190            0.0.0.0:*               LISTEN      610761/dovecot
  tcp        0      0 0.0.0.0:110             0.0.0.0:*               LISTEN      610761/dovecot
  tcp        0      0 0.0.0.0:24              0.0.0.0:*               LISTEN      610761/dovecot
  tcp        0      0 0.0.0.0:143             0.0.0.0:*               LISTEN      610761/dovecot
  ```

#### Enable and configure AUTH and LMTP
  * All dovecot services are managed through a service configuration file named **10-master.conf**.
  * Two services were mentioned in [[Postfix SMTP server tutorial]] which are to be configured in Dovecot, AUTH and LMTP. 
  * LMTP by default runs on port 24. We will change that to a unix socket. 

  ```
  service lmtp {
    unix_listener /var/spool/postfix/private/dovecot-lmtp {
      mode = 0600
      user = postfix
      group = postfix
    }
  }
  
  service auth {
    unix_listener /var/spool/postfix/private/auth {
      mode = 0600
      user = postfix
      group = postfix
    }
  }
  
  ```

  * Authentication is configured in **10-auth.conf**. Dovecot supports multiple user databases such as LDAP, MySQL, Static, can use System users(users /etc/passwd file) etc. We will use LDAP in this tutorial.
  * Replacing the default configuration with one written below is enough to get started. Personally, i keep a copy of default config. This makes it easier to check and add additional options if necessery without checking dovecot documentation. The default config also has a lot of info and links to documentation specific to config file.  

  ```
  disable_plaintext_auth = yes
  auth_username_format = %Lu
  auth_username_chars = abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890.-_@
  auth_mechanisms = plain login
  
  ## Password and user databases
  !include auth-ldap.conf.ext
  ```

###### Configure LDAP User Database
  * As mentioned in **10-auth.conf**, LDAP is user database. Inorder to configure authentication with LDAP,  **dovecot-ldap.conf** is configured.

  ```
  hosts = localhost
  auth_bind = yes
  auth_bind_userdn = uid=%n,ou=users,dc=example,dc=in
  base = dc=example,dc=in
  scope = subtree
  user_attrs = homeDirectory=home
  # By default, dovecot reads quota attribute in bytes. Uncomment following in such case
  # user_attrs = homeDirectory=home,mailQuota=quota_rule=*:bytes=%$
  # In case you want to read mail quota attribute in MB, uncomment following.
  # user_attrs = homeDirectory=home,mailQuota=quota_rule=*:storage=%$M
  user_filter = (&(objectClass=inetOrgPerson)(mail=%s))
  pass_attrs = uid=user,userPassword=password
  pass_filter = (&(objectClass=inetOrgPerson)(mail=%s))
  ```

  * More info about Dovecot LDAP configuration can be found in [link](https://doc.dovecot.org/configuration_manual/authentication/ldap/).

###### Configure LMTP 
  * Mail is delivered to mailbox with Local Mail Transfer Protocol (LMTP). 20-lmtp.conf is the configuration file for LMTP.

  ```
  # If quota plugin is used remove uncomment following parameter.
  # lmtp_rcpt_check_quota = yes
  lmtp_add_received_header = yes
  protocol lmtp {
    # Without quota plugin uncomment following
    mail_plugins = $mail_plugins sieve
    # With quota plugin uncomment following
    # mail_plugins = $mail_plugins quota sieve
  }
  ```


#### Configure Mailbox
  * Mailbox configuration and folder permission is configured next. Following files contains mailbox related configuration options
    - 10-mail.conf: Mail directory format, owner, group configuration options etc are present. 
    - 15-mailboxes.conf: Each Mailbox in user directory is configured. We can subscribe/enable both default and custom folders, can create additional folders etc for users.

  * **10-mail.conf**
    - Mailbox format is set to **maildir**. Dovecot supports multuple [mailbox format](https://doc.dovecot.org/admin_manual/mailbox_formats/#mailbox-formats).
    - Mailbox location, owner(file permission on system) are set. 
    - **dovecot** user and group is set as owner of all the mailboxes. 

  ```
  mail_location = maildir:/home/%n/Maildir/
  #mail_location = maildir:/home/example.com/%n/
  # In this case dovecot will use home value mentioned in user db(LDAP)
  #mail_location = maildir:%{userdb:home}/Maildir
  
  namespace inbox {
    inbox = yes
  }
  
  mail_uid = dovecot
  mail_gid = dovecot
  first_valid_uid = dovecot
  mail_privileged_group = dovecot
  ```

  * 15-mailboxes.conf
    - Add **auto** param to file as shown in example below. It indicates whether the folder should be created automatically or not. It has 3 possible values: 
      no        : Never created automatically.
      create    : Automatically created, but no automatic subscription.
      subscribe : Automatically created and subscribed.

  ```
  # auto:
  #   Indicates whether the mailbox with this name is automatically created
  #   implicitly when it is first accessed. The user can also be automatically
  #   subscribed to the mailbox after creation. The following values are
  #   defined for this setting:
  #
  #     no        - Never created automatically.
  #     create    - Automatically created, but no automatic subscription.
  #     subscribe - Automatically created and subscribed.
  # NOTE: Assumes "namespace inbox" has been defined in 10-mail.conf.
  
  namespace inbox {
    # These mailboxes are widely used and could perhaps be created automatically:
    mailbox Drafts {
      auto = subscribe
      special_use = \Drafts
    }
    mailbox Junk {
      auto = subscribe
      special_use = \Junk
    }
    mailbox Trash {
      auto = subscribe
      special_use = \Trash
    }
  
    mailbox Sent {
      auto = subscribe
      special_use = \Sent
    }
    mailbox "Sent Messages" {
      auto = subscribe
      special_use = \Sent
    }
  }
     
  ```
 
#### Configure TLS
  * TLS is used to enable secure IMAP or POP3 connection. **10-ssl.conf** is the TLS configuration file.

  ```
  ssl = yes
  ssl_cert = </etc/letsencrypt/live/example.com/fullchain.pem
  ssl_key = </etc/letsencrypt/live/example.com/privkey.pem
  ssl_min_protocol = TLSv1.1
  ```

  * Once TLS is set up, IMAPS & POP3S can be enabled by uncommenting service in **10-master.conf**. For IMAPS, uncomment following in **imap-login** service. Similarly, POP3S can also be enabled. 

  ```
  inet_listener imaps {
    port = 993
    ssl = yes
  }
  ```

#### Configure Logging
  * Inorder to enable logging in dovecot we need to set **log_path** parameter in **10-logging.conf**. It can have 3 possible values:
    - syslog: logs to syslog
    - /dev/stderr: logs to stderr
    - /some/path/to/dovecot.log: Dovecot log at user defined location. 
  * Syslog is generally preferred as os takes care of logs and rotate older logs etc. 
  * More info about logging verbosity can be found in [link](https://doc.dovecot.org/admin_manual/logging/#logging-verbosity).

  ```
  log_path=syslog
  ```

#### Configure sieve

  * Sieve filter examples can be found in [Pigeonhole Sieve examples](https://doc.dovecot.org/configuration_manual/sieve/examples/).
  * Seive is an email filtering language for email filtering.
  * It is an internet standard, hence t is not tied to any particular operating system or mail architecture.
  * Sieve specifications can be found in [link](http://sieve.info/documents).
  * Enable managesieve in **20-managesieve.conf**

  ```
  protocols = $protocols sieve
  service managesieve-login {
   inet_listener sieve {
      port = 4190
    }
  }
  service managesieve {
      process_limit = 1024 
  }
  ```

  * Configure sieve filter location in **90-sieve.conf**.

  ```
  plugin {
    sieve = file:/home/vmail/%n/sieve;active=/home/vmail/%n/.dovecot.sieve
    }
  ```
  
  * Inorder to enable sieve plugin add **sieve** to **mail_plugins** param in **20-lmtp.conf**
  
  ```
  mail_plugins = $mail_plugins sieve
  ```


A Dovecot server with IMAP(S), POP3(S) have been set up with LMTP for mail delivery and Sieve for mail filtering.
