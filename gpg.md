---
layout: page
title: gpg
---
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGD21ZYBEADCko6e9EK1EpSIE6NfD+nh2EvPdJKZxMPC0aCZuYNdgDQO7diw
jteoxhkas1dQIUTn25DACoVutJoGaJwdKgLDTyStMRRDe0WPXHj+N3LzAlAcVvDD
0XM7GrA6/X6IAncv7Z7Lfqenp94NCCbG2u0DPQPPAsmW5GxdpoAzdRlwBoQKlUau
v9nXy0Mg/e8522EZfI1Iz8xDGnI11pmjmXAfg5Xm1+a+rQxJ26he/AOEAwCphKfI
QKBWLn15BjeSKyjTJAtczNKQf7C5iKdxp0KuyKVQyPba9ZSv9BzM0dsrREt06mDs
UaFbwsMArA0DdQct/CFc/JwRyzVS1dnKp3X/8Z+zG8krOOrfHjEBXFXnvvg0PkjS
n6uHCZGd+cOqIzO6UE/14trD5vg5gfCW6eEUhgAW3N94V75xENhSkLoP9VafDTln
7HOWRSQ00kdtPDJecc+giM0BF/jKZLz5NPuXIrcwZB/o/Q7JKRDbKe/vfqDHckCq
5KlmeAHi0oNzPWgXCIQEHgMm69P1S1oyf5Y9NCeqiiUv9rMhwfRDH9wUsXfkLqyq
2jZR7c+ek6mmRGrNRRXxd355aIjzoR/2wsw4cTGPZmPzDd6ANDg0HS6Z2R3Aencm
l2w7SFkW+Mav6qQB4OiS6uBG1XBX0Lsu+d8UHSyexXQFwGZ0xjiO8IP4AwARAQAB
tChBa3NoYXkgUHVzaHBhcmFqIDxha3NoYXlAbXJ0c3VraW1vdG8uaW4+iQJUBBMB
CgA+FiEE4OhoqJKSKVPpcjGBodk1xakhxGoFAmD21ZYCGwMFCQHhM4AFCwkIBwIG
FQoJCAsCBBYCAwECHgECF4AACgkQodk1xakhxGrU3g//V4Azpw4e25YOj+C/bIo+
00R06HYk58HJPCc8DXPtowVIbVnlYZbkFo3n7DVRrK82/0zYXFCn++UU301OyHU2
wsLSeOP4DaFg+gGJFx7fRit1T/0mehbqa2xo+gK0EEzl54gwSAZ1ey19PeNjj3MY
7/8q4iBe9Ivu+9xRsge7EgqyYg2mimhhyYzRKlZHfz0Qaaq1+ui2ihWwwjoIFqMm
cC//zpUt7Mt2q6YpJdAW19KpkS/oFrAiRMJ2wGwXCFTaEY157Qczc58plxHkF4Ml
DXIjFogxXUOvKBUjvdKdrG4BN+hDCKca3ILmxnZ0BpfRmhBvQQDz2hgPr/zZCbuj
wbPPrFsMkZmPWJEz7YBPbu4QMfCtPmUsDdddgm2WGErslQJbtgCZlqYQN1QIwhiq
Du5cG3hkqVXQrf2LrMF1HoPnSQUNzpfZNdRbu1EsPm7/zF2s+vAgzlxvSdl9yFFw
TCRO1gVg7JW76QuyjSmTKI+Tdmz2F2HbA0lKHBm9HShsTAX/1NAOf2MlKQ2jgh5O
6UU5s0cvJwVdpogUJZ09zEg7KGF74qKFgHCS72+TXCGvsGbnUOsHBKcwsH4OOoSp
clJfDJ0TYKX+Y0pxJt+PD1CwL0qx76lG/e2GsZSXJoJNgCf0Zq2cWEOBJ7Sc/sQe
bNLLdA9AkkxBZMcrsdgJAFy5Ag0EYPbVlgEQANlQ03PZBpfotPv6h9+V88f9bwg+
CR4FBF00BkLEhY5BsERR6YAwAelK70Yn+KOF5YogNhLav03uh2ykHEwhpoBDkIbI
TLl9oVkoiXfBS/y25OFMwkWL/Zk1cuxl4VDMx7ZQ2CsgH/rVBtit8dKWdw4JRxq4
n9r2DxKqFirIdfyigHm2QBMPWh/SmmMZwT7EsgU/wl36u7YNsFUWEJ4VWe+ZEjQs
N8EUNXX0GaDRb/1Z7INiTP60OsqyGpMrtC/DvSCwvyE9wCW82SFgt13lu0acr428
GQl5XJEgWhZal8/wiDykh3guXqUOyUMxP1rv0/7y00qpjc2sTC1wL3dFQwpvmeMA
cZclJxsmjanlFaAs3pjw3dI/5C2EtelNfIpqdNCz9OJcwDgioUpKVfh+ELbaqLml
IhvXuhADAziXYVLE/no7at0BBTjzHlgNgA569fRXNs5hhCDBzYL2K0DwRhwVEy59
hMrjh8jfwPTmQaEIzo7LiGc5Z5FH2Tb4LhRICecdjWwYz0GGnvUWI/Oow4BrzJSv
zAxnFj07FAjwFjeycYSuRsIFvN/IKf3JURqZSl4GPfeaVhQ7OPzN9UVbsaP832LH
Shz5FNbs80chCF5XGtC8TlBzYGLDQLsPfNbOXFoTxJ6Zq++OPUaqrvpBhhLNUbqO
NdeHX+3qk1+5+UlTABEBAAGJAjwEGAEKACYWIQTg6GiokpIpU+lyMYGh2TXFqSHE
agUCYPbVlgIbDAUJAeEzgAAKCRCh2TXFqSHEal8lD/0eo9dP4ukifwGl8z+witix
tnHeMmugX3X8gXZLQCWMW8GDPfMXTylZkcP90kivKVI+NWJOj3MJsTlEo8uVWEjb
8KP+QQVNEqVG94PyQwgriTX4/kPeetbcrbsoXxEaGe/bR4Szo8BXxc7/rxlmal93
tT/TDtJ5R/BjykITlhCF31Tzo1A/GDD0Fh23U9BY5X0e3MTA8M6ZyHNn+SPQcaF1
e283rUsUshJ7fqj9ZzHBLXjrFgbod35wQ+3yXl+MFlzdDRGUhJWtmwjgbI4mD9SZ
MyPy0I/QBLta+VHc/YJpRVPSGpG30q+p6b+WUbHMPVmsDpWVitgUt5ss1V4zemqT
9/jJX0OmCOe6b0CGA9tRedXJwPkHxtpgZ5vsSyHEkVulUHwtelJ4YQ+bcvUm5Ib0
Vj4X4HCR+nw04pYqGpsnEtQ29NGaxKk7G5q0/iL1LZ8vsiwyjatHTa7ZFh+KGHNK
ORLvnFNB2y1ulnQMtliwY+SNS0WbQ0MS9N5GadLp86u0iTqmvJB4XbUeEfIswF0X
sNEQvad0L8j0rK1zwvbvvxYwupLPB9qgYFzLCVIoUQtYMdxIZg1Aq6bANtIlWtoG
y4RLalANzzbZYKVcUGM3OEOaPy2++2d2iSbvY5Xd5eW6yYwDnyFm8nOpBtzk2Zd0
txeuqwZgWHLgq3d2dj1qhQ==
=cQIL
-----END PGP PUBLIC KEY BLOCK-----
