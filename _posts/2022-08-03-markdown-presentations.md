---
layout: post
title: Markdown presentation using pandoc
date: 2022-08-03 18:15 +0530
---

Use pandoc to convert markdown to presentation. Inorder to install pandoc
```
sudo apt install pandoc texlive texlive texlive-base texlive-xetex texlive-extra-utils 
```

create a markdown file.
```
---
title: Test Presentation
author: Test Author
---
# Test Section
- test
![Image](gnu.png)

# Test 2 Section
## Test 2 Subsection
- test 2
  - test 2 subpoint
    - test 2 sub-subpoint
```
convert markdown file 
```
pandoc -t beamer test.md -o test.pdf
```
<br/>
<object data="{{ site.url }}{{ site.baseurl }}/pdf/test.pdf" width="1000" height="500" type="application/pdf"></object>

Heres a pdf i created using jekyll:
<object data="{{ site.url }}{{ site.baseurl }}/pdf/openldap.pdf" width="1000" height="500" type="application/pdf"></object>
The source code for this presentation can be found [here](https://sovran.dev/akshay/openldap)

Slides can be customized with [Beamer themes](https://latex-beamer.com/tutorials/beamer-themes/). A really helpful guide i formarkdown presentation can be found in this <a href='https://github.com/alexeygumirov/pandoc-for-pdf-how-to'>link</a>. 

