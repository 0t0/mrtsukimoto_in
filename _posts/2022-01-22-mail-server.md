---
layout: post
title: Mail Server with Postfix & Dovecot
description: Setting up a self hosted mail server with postfix and dovecot
date: 2023-09-21 00:06 +0530
---

<h2> Settiing up services</h2>
{%- if site.mailserver.size > 0 -%}
  <ul>
    {%- for post in site.mailserver -%}
    <li>
      <a href="{{ post.url | relative_url }}">{{ post.title | escape }}</a>
    </li>
    {%- endfor -%}
  </ul>
{%- endif -%}
<br/>
I have also written an [ansible playbook](https://gitlab.com/mrtsukim0t0-/postfix-playbook). Do check if interested.

# Original project links
* SMTP - [Postfix](http://www.postfix.org/) 
* IMAP - [Dovecot](https://www.dovecot.org/documentation/)
* User Database - [OpenLDAP](https://openldap.org)
