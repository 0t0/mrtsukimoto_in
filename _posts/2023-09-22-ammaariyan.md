---
layout: post
title: "Thoughts: John Abraham & Amma ariyan(1986)"
date: 2023-09-22 02:45 +0530
---
![John Abraham](/assets/johnabraham.jpg)
John abaraham is a legend, but what makes him standout from the crowd is the Odessa Collective. Odessa Collective was an attempt by a group of movie enthusiasts to change the history of film production and distribution by making it a collaborative effort with the public and thus act as an empowering and liberating medium. He believed in and understood the power of this cinema and stories like these are inspiring.

```
  Cinema is not a collective art. A film directed by John Abraham is entirely of his own, just John Abraham’s.
  I take the decisions (not others). I am the Hitler of my cinema
```

Amma ariyan is one of the greatest malayalam film ever made.

Not mentioning Kummatty(1979) in the same breath would be a crime, but it is well known. G. Aravindan's work is very well know, recently Scorcese's [The film foundation](https://www.film-foundation.org/) restored [Thampu](https://en.wikipedia.org/wiki/Thampu). Aravindan requires a post of his own, so i'll get back to the matter at hand.  

One thing that i had noticed with the film is the amount of constraint in it. Everyone seems to be in a struggle with one's own ideals & emotions. They do things because they have to. The film depicts this hopelessness and inner turmoil, being shot it B&W added to it. The characters rarely talk, they do what needs to be done. This is not a flashy film in any way, it's a bare bone film and it needs to be one too and found the experience of watching Amma ariyan thought provoking and extremely satisfying.

I do not have enough words to appreciate such a wonderful film that requires a lot more attention than it currenly has. I sure do hope a lot more people watch it.
The best thing is that its available for free in [youtube](https://youtu.be/bpsY0J010uc?si=qdsR5x7awi0X2A2G). Oh and its [Joy Mathew's](https://en.wikipedia.org/wiki/Joy_Mathew) debut film. 


