---
layout: post
title: 'ddcutil: How to swtich between two desktops connected to VGA and HDMI inputs of single monitor'
description: 'Switching between desktops connected to the VGA and HDMI inputs of monitor.'
date: 2022-06-04 12:07 +0530
---

One day at work i was learning something and i was looking back and forth at my thinkpad and desktop. i had been at it for a while and enough was enough. So i searched for tools that would let me use two  different devices on a single monitor. This was also due to the thinkpad having a smaller resolution and it was a little cramped in there. Finally i came to know about ddccontrol and then ddcutils. ddccontrol is not in development anymore. It was succeded by ddcutils. 

So what we can do here is switch input source of monitor between VGA and HDMI.

We can get the serial numbers and other info about all the monitors connected to the system with the below command. 
```
➜ ~ sudo ddcutil detect

Display 1
   I2C bus:             /dev/i2c-2
   EDID synopsis:
      Mfg id:           AOC
      Model:            24B1W1
      Serial number:    WWW22K0800358
      Manufacture year: 2019
      EDID version:     1.3
   VCP version:         2.2
```

<br/>

To list all the things that can be done with ddcutils like brightness control, etc.
```
➜ ~ sudo ddcutil capabilities

...
   Feature: 60 (Input Source)
      Values:
         01: VGA-1
         11: HDMI-1
...
```

And finally,
To switch to HDMI
```
➜ ~ sudo ddcutil -n WWW22K0800358 setvcp 0x60 0x11
```

To switch to VGA
```
➜ ~ sudo ddcutil -n WWW22K0800358 setvcp 0x60 0x01
```
