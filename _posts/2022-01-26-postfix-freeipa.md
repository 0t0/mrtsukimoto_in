---
layout: post
title: Freeipa group expansion for postfix
date: 2023-09-21 00:43 +530
tag: freeipa, ldap, postfix, groups, groupexpansion
---

This tutorial will present reader 2 ways to add mail to a group and how we can configure Postfix for group expansion. 

FreeIPA Groups does not allow mail attribute to group by default, which means that we must add **mailGroup** objectClass to the group. Consider an example group named support. To add mail attribute to group run the following command 

``` 
  ipa group-mod support --addattr="objectclass=mailGroup" --addattr="mail=support@example.com" 
```

This will add **mailGroup** objectclass  and mail attribute with value "support@example.com"

```
  ####support, groups, accounts, amogha.labnetwork.in
  dn: cn=support,cn=groups,cn=accounts,dc=example,dc=com
  cn: support
  objectClass: top
  objectClass: groupofnames
  objectClass: nestedgroup
  objectClass: ipausergroup
  objectClass: ipaobject
  objectClass: mailGroup
  ipaUniqueID: 58c0bc3a-7e97-11ec-ab9c-96f26bde8936
  mail: support@example.com
  member: uid=test,cn=users,cn=accounts,dc=example,dc=com
  member: uid=test2,cn=users,cn=accounts,dc=example,dc=com
```

 In postfix, create a ldap lookup table. I usually create all these ldap files in /etc/postfix/ldap
 Inorder to use this mail group with Postfix, we have to configure 2 things:
  - A lookup table that will return members of group
  - Adding this lookup table to **main.cf** config file. 

LDAP Group lookup table (/etc/postfix/ldap/virtual_groups.cf)

```
    bind = yes
    bind_dn = uid=user,cn=users,cn=accounts,dc=example,dc=com
    bind_pw = secret
    server_host = ipa.example.com
    search_base = cn=accounts,dc=example,dc=com
    query_filter = (&(objectClass=groupofnames)(mail=%s))
    leaf_result_attribute = mail
    special_result_attribute = member
```

We use **leaf_result_attribute** instead of **result_attribute** because using latter will also return groups email id. This can be checked with postmap command as given below. Former uses result_attribute, latter leaf_result_attribute.

```
  $ postmap -q support@example.com ldap:/etc/postfix/ldap/virtual_groups.cf
  support@example.com,test@exmaple.com,test2@exmample.com
 
  $ postmap -q support@example.com ldap:/etc/postfix/ldap/virtual_groups.cf
  test@exmaple.com,test2@exmample.com
```

To add group expansion to postfix, append **ldap:/etc/postfix/ldap/virtual_groups.cf** to **virtual_alias_maps**.

```
    vitual_alias_maps = $(whatever user configured), ldap:/etc/postfix/ldap/virtual_groups.cf
```

<br/>
And we have successfully configured group expansion for FreeIPA in Postfix. But... adding objectClasses using cli is bit of a hassle and we will not be able to see group mails in UI. 

What can we do here? The simplest option is to use an attribute that is already shown in UI, wouldn't it?  Enter **description** attribute. No extra work, shows up in UI.

Is it ugly to see mails under description? Yes, atleast we don't need to use cli. I consider this a win-win.

How can we do this? Replace **mail** in lookup table above with **description**. Done. 
 
```
    bind = yes
    bind_dn = uid=user,cn=users,cn=accounts,dc=example,dc=com
    bind_pw = secret
    server_host = ipa.example.com
    search_base = cn=accounts,dc=example,dc=com
    query_filter = (&(objectClass=groupofnames)(description=%s))
    leaf_result_attribute = mail
    special_result_attribute = member
```
Finally! we successfully configured group expansion for FreeIPA in Postfix.
