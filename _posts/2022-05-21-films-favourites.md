---
layout: post
title: Films, favourites.
description: "A list of favourites: Movies"
date: 2022-05-21 13:40 +530 
---

- [Akira](https://en.wikipedia.org/wiki/Akira_(1988_film))
- [Apocalypse Now](https://en.wikipedia.org/wiki/Apocalypse_Now)
- [Blade Runner](https://en.wikipedia.org/wiki/Blade_Runner)
- [Blade Runner 2049](https://en.wikipedia.org/wiki/Blade_Runner_2049)
- [Children Of Men](https://en.wikipedia.org/wiki/Children_of_Men)
- [Cinema Paradiso](https://en.wikipedia.org/wiki/Cinema_Paradiso)
- [Ee.Ma.Yau](https://en.wikipedia.org/wiki/Ee.Ma.Yau.)
- [Eternal Sunshine Of a Spotless Mind](https://en.wikipedia.org/wiki/Eternal_Sunshine_of_the_Spotless_Mind)
- [Fight Club](https://en.wikipedia.org/wiki/Fight_Club)
- [Ghost in the Shell](https://en.wikipedia.org/wiki/Ghost_in_the_Shell_(1995_film))
- [In the mood for love](https://en.wikipedia.org/wiki/In_the_Mood_for_Love)
- [Mad Max: Fury Road](https://en.wikipedia.org/wiki/Mad_Max:_Fury_Road)
- [Mind Game](https://en.wikipedia.org/wiki/Mind_Game_(film))
- [Mononoke Hime](https://en.wikipedia.org/wiki/Princess_Mononoke)
- [Moonlight](https://en.wikipedia.org/wiki/Moonlight_(2016_film))
- [Paparika](https://en.wikipedia.org/wiki/Paprika_(2006_film))
- [Piku](https://en.wikipedia.org/wiki/Piku)
- [Roshomon](https://en.wikipedia.org/wiki/Rashomon)
- [Spirited Away](https://en.wikipedia.org/wiki/Spirited_Away)
- [Stalker](https://en.wikipedia.org/wiki/Stalker_(1979_film))
- [Synecdoche, New York](https://en.wikipedia.org/wiki/Synecdoche,_New_York)
- [Taxi Driver](https://en.wikipedia.org/wiki/Taxi_Driver)
- [The Grant Budapest Hotel](https://en.wikipedia.org/wiki/The_Grand_Budapest_Hotel)
- [The Master](https://en.wikipedia.org/wiki/The_Master_(2012_film))
- [The Matrix](https://en.wikipedia.org/wiki/The_Matrix)
- [The Thin Red Line](https://en.wikipedia.org/wiki/The_Thin_Red_Line_(1998_film))
- [The Tree Of Life](https://en.wikipedia.org/wiki/The_Tree_of_Life_(film))
- [There Will Be Blood](https://en.wikipedia.org/wiki/There_Will_Be_Blood)
- [Thondimuthalum Driksakshiyum](https://en.wikipedia.org/wiki/Thondimuthalum_Driksakshiyum)

