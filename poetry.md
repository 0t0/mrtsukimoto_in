---
layout: page
title: Poetry
---
<a href="https://mrtsukimoto.in/feed/poetry.xml">Poetry Feed</a>
<h6>Poems</h6>
{% assign poetry = site.poetry | sort: 'date' | reverse %}
{%- if poetry.size > 0 -%}
  <ul>
    {%- for post in poetry -%}
    <li>
      {%- assign date_format = "%d %B,%Y" -%}
      [ {{ post.date | date: date_format }} ] <a href="{{ post.url | relative_url }}">{{ post.title | escape }}</a>
    </li>
    {%- endfor -%}
  </ul>
{%- endif -%}
